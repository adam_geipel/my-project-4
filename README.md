# CS441/541 Project 4

## Author(s):

Adam Geipel


## Date:

April 7th, 2016


## Description:




## How to build the software

Run the makefile to compile the file scheduling_sim.c

CC = gcc
CFLAGS = -Wall -g -O0 - I../lib
LDFLAGS = -pthreads

Make clean will clean up the folder.


## How to use the software




## How the software was tested

 


## Known bugs and problem areas
