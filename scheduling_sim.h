/*
 * 
 * CS 441/541: Scheduling Simulator
 * By Adam Geipel
 * April 7th, 2016 
 *
 */
#include <stdio.h>
#include <stdlib.h>

/*****************************
 * Defines
 *****************************/


/*****************************
 * Structures
 *****************************/
struct sim_session {
	// Represents the scheduling algorithm to be simulated 
	int algo;
	
	// Represents the time quantum for Round Robin scheduling
	int quantum;
	
	//Hold all processes in an array so they're easy to deal with
	int num_processes;
	struct process * processes;
};

struct process {
	int pid;
	int burst_len;
	int	priority;
};
	

/*****************************
 * Global Variables
 *****************************/


/*****************************
 * Function Declarations
 *****************************/

// Initialize session variables
struct 
sim_session * makeNewSession();

// Free up memory when we're done
void
freeSession(struct sim_session * session);

// Add processes to session->processes
void
addProcess(struct sim_session * session, int pid, int burst, int priority);

// After reading the number of processes, make room for them
void 
allocate_for_processes(struct sim_session * session, int process_count);

// Given global filename, read file and make processes
void 
read_and_evaluate(struct sim_session * session, const char * fileName);

// Take the number that corresponds with the algorithm and get the name as a string
char * 
toString(int value)

/* ******** TEST FUNCTIONS DECLARED HERE ******** */

void
test_FCFS(struct sim_session * session);

void
test_SJF(struct sim_session * session);

void
test_priority(struct sim_session * session);

void
test_rr(struct sim_session * session);

