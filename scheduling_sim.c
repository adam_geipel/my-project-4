/* 
 * Scheduling Simulator
 * 
 * By Adam Geipel
 * For CS441
 *
 * April 7th, 2016
 *
 * Simulates various scheduling algorithms based on text file input 
 *
 */
#include "scheduling_sim.h"

int main(int argc, char * argv[]) 
{
	
	struct sim_session * session = makeNewSession();    

	int i;

	const char * fileName;
    
    for (i = 1; i < argc; i++)
	{
		// Get the Scheduling Algorithm from the command line
        if (argv[i] == "-s")
		{
			
            if ((int)strtol(argv[i+1], NULL, 10) != 1  && (int)strtol(argv[i+1], NULL, 10) != 2  
				&& (int)strtol(argv[i+1], NULL, 10) != 3 && (int)strtol(argv[i+1], NULL, 10) != 4)
			{
				fprintf(stderr, "Error: Invalid scheduling algorithm identifier");
				return 0;
			}
			else 
			{
				session->algo = (int)strtol(argv[i+1], NULL, 10);
				i++;
			}
			
        }
	
		if (argv[i] == "-q")
		{
			session->quantum = (int)strtol(argv[i+1], NULL, 10);
			i++;
		}

		
		else 
			fileName = (const char *)argv[i]; 
	}

	read_and_evaluate(session, fileName);

	printf("Scheduler	:  %d %s\n", session->algo, toString(session->algo);
	printf("Quantum		:  %d\n", session->quantum);
	printf("Sch. File	:  %s\n", fileName);
	printf("------------------------------------\n");
	
	/*
	 * Because the processes were put into the array as they were read
	 * We know the arrival order
	 * So a simple for loop can help print process information
	*/
	printf("Arrival Order: ")
	for(i = 0; i < session->num_processes; i++)
		printf("%d, "session->processes[i]->pid);
	printf("\n");
	
	printf("Process Information: \n")
	for(i = 0; i < session->num_processes; i++)
	{
		printf("%-3d\t"session->processes[i]->pid);
		printf("%-3d\t"session->processes[i]->burst_len);
		printf("%-3d\t"session->processes[i]->priority);
	}
	printf("\n");
	printf("------------------------------------\n");
	printf("Running...");


	return 0;
}


struct sim_session *
makeNewSession() 
{
	struct sim_session * session = malloc(sizeof(struct sim_session));
	if (!sim_session)
		fprintf(stderr, "Error: Session failed to be created.\n");
	
	session->algo = 0;
	
	/* 
	 * Not really necessary unless doing round robin
	 * Good to have just in case
	*/		
	session->quantum = 0;	

	//initialize process counter and array to keep track of processes
	session->num_processes = 0;
	session->processes = malloc(sizeof(struct process));
	if (!processes)
	{
		fprintf(stderr, "Error: Failed to initialize an array to hold processes\n");
		return NULL;
	}

	return session;
}


void
freeSession(struct sim_session * session)
{	
	// Free each process in the array of processes
	for (int i = 0; i < session->num_processes; i++)
		free(session->processes[i];

	// Free processes array
	free(session->processes);
	free(session);
}

void
addProcess(struct sim_session * session, int pid, int burst, int priority)
{
	struct process * process = malloc(sizeof(struct process));
	if (!process)
	{
		fprintf(stderr, "Error: Failed to add process\n");
		return NULL;
	}
	
	process->pid = pid;
	process->burst_len = burst;
	process->priority = priority;

	// Add process to pool
	session->num_processes++;
	session->processes[num_processes] = process;
}

void 
allocate_for_processes(struct sim_session * session, int process_count)
{
	session->processes = realloc(session->processes, sizeof(struct process) * (process_count + 1));
}

void 
read_and_evaluate(struct sim_session * session, const char * fileName)
{
	
	FILE *fp;

	// How many processes will we have?
	int process_param;
	
	// ints needed to assign properties to processes
	int pid, burst, priority;

	fp *fopen(fileName, "r");
	
	// first line of file: lets us know how many processes we will be working with
	fscanf(fp, "%d", &process_param);

	// Allocate processes array in session
	allocate_for_processes(session, process_param);

	// Read in each line of file to get process information	
	while (!EOF)
	{		 
		fscanf(fp, "%d %d %d", &pid, &burst, &priority); 
		
		addProcess(session, pid, burst, priority);	
	}

	fclose(fp);
}

char * 
toString(int value)
{
	if (value == 1)
		return "FCFS";
	else if (value== 2)
		return "SJF";
	else if (value == 3)
		return "Priority";
	else
		return "Round-Robin (RR)";	
}
	
